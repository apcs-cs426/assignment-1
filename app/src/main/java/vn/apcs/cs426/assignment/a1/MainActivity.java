package vn.apcs.cs426.assignment.a1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.util.Date;
import java.util.List;

import vn.apcs.cs426.assignment.a1.data.CinemaShowtimeRepository;
import vn.apcs.cs426.assignment.a1.data.DummyCinemaShowtimeDataSource;
import vn.apcs.cs426.assignment.a1.data.model.MovieInfo;
import vn.apcs.cs426.assignment.a1.data.model.Showtime;

public class MainActivity extends AppCompatActivity {

  private final String TAG = this.getClass().getSimpleName();

  DummyCinemaShowtimeDataSource dataSource;
  CinemaShowtimeRepository dataRepository;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    dataSource = DummyCinemaShowtimeDataSource.getInstance();
    dataRepository = CinemaShowtimeRepository.getInstance(dataSource);

    //    get the sample movie information data from data repository
    MovieInfo movie = dataRepository.getMovieInfo();
    Log.d(TAG, String.format("Movie title: %s", movie.getMovieTitle()));

    //    get list of date from data repository with start date. It will get 10 dates from dummy
    // data source
    Date startDate = new Date(2019, 05, 28);
    List<Date> listDates = dataRepository.getListDates(startDate);
    for (Date date : listDates) {
      Log.d(
          TAG,
          String.format("d/M/y = %d / %d / %d ;", date.getDate(), date.getMonth(), date.getYear()));
    }

    //    get list of showtimes from data repository with starting time. It will get 10 showtimes
    // from dummy data source
    Date startTime = new Date(0, 0, 0, 10, 30);
    List<Showtime> listTimes = dataRepository.getListShowtimes(startTime);
    for (Showtime time : listTimes) {
      Log.d(
          TAG,
          String.format(
              "h:m = %d : %d; is available = %b",
              time.getTime().getHours(), time.getTime().getMinutes(), time.isAvailable()));
    }
  }
}
