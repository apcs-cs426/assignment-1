package vn.apcs.cs426.assignment.a1.data.model;

import java.util.Date;

public class Showtime {
  private Date time;
  private boolean available;

  public Showtime(Date time, boolean available) {
    this.time = time;
    this.available = available;
  }

  public Date getTime() {
    return time;
  }

  public boolean isAvailable() {
    return available;
  }
}
